FROM ubuntu:22.04 as builder

ARG ZOLA_VERSION="0.17.1"
ADD https://github.com/getzola/zola/releases/download/v${ZOLA_VERSION}/zola-v${ZOLA_VERSION}-x86_64-unknown-linux-gnu.tar.gz /tmp/zola.tar.gz

RUN tar -xzf /tmp/zola.tar.gz -C /tmp \
    && mv /tmp/zola /usr/local/bin/zola

FROM ubuntu:22.04

COPY --from=builder /usr/local/bin/zola /usr/local/bin/zola

WORKDIR /project

COPY . .

