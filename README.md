# Personal Website

This project is a personal website generated using [Zola](https://www.getzola.org/) with a template, which I have modified and deployed on AWS using Apache2 as the web server.

Website URL: [http://18.218.69.233/](http://18.218.69.233/)

## About (ABOUT)

This section contains my personal information.

![图1](微信截图_20240129211324.png)

## Education (EDUCATION)

Here, I showcase my academic background, including degrees earned and major courses undertaken.

![](微信截图_20240129211348.png)

## Skills (SKILLS)

This part outlines my skills and expertise in software development and other related fields.

![](微信截图_20240129211401.png)

## Projects (PROJECTS)

This section presents some of my project works from the IDS721 course, demonstrating my practical abilities in data science and software development.

## Deployment

The website is automatically deployed to Vercel through the GitLab CI/CD pipeline configured in .gitlab-ci.yml file. The workflow includes the following steps:

**Build**: On each push to the repository, GitLab CI/CD runs a job from a image put on Docker Hub that builds the static site using Zola.

GitLab CI/CD:

```yaml
image: 
  name: raplis/my_website:latest
  
pages:
  script:
    - zola build
  artifacts:
    paths:
    - public

```

Dockerfile:

```dockerfile
FROM ubuntu:22.04 as builder

ARG ZOLA_VERSION="0.17.1"
ADD https://github.com/getzola/zola/releases/download/v${ZOLA_VERSION}/zola-v${ZOLA_VERSION}-x86_64-unknown-linux-gnu.tar.gz /tmp/zola.tar.gz

RUN tar -xzf /tmp/zola.tar.gz -C /tmp \
    && mv /tmp/zola /usr/local/bin/zola

FROM ubuntu:22.04

COPY --from=builder /usr/local/bin/zola /usr/local/bin/zola

WORKDIR /project

COPY . .

RUN zola build
```



**Deploy**: Upon a successful build, the site is deployed to Vercel, where it is hosted.

![image-20240306121331224](image-20240306121331224.png)

 
